package com.ubestkid.kidrhymes;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.StrictMode;


import androidx.multidex.MultiDex;


import com.lzy.okgo.OkGo;

import com.lzy.okgo.model.HttpHeaders;


import com.ubestkid.kidrhymes.constant.Constants;
import com.ubestkid.kidrhymes.presenter.STAdManager;
import com.ubestkid.kidrhymes.utils.LogUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreator;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreator;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;




public class AppApplication extends Application {

    public static AppApplication mContext;
    public String mVersionName;
    public int mVersionCode = 0;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

//        getAppVersion();

//        STAdManager.getInstance().initSdk(Constants.APP_ID, this, true,false);
//        TAdManager.init(this, new TAdManager.AdConfigBuilder()
//                .setAppId(Constants.APP_ID)
//                .testDevice(false)
//                .setDebug(true)
//                .build());

//        Bugly.init(getApplicationContext(), "c9458e39dc", false);
    }

    private void initFileProvider() {
        // 解决7.0以上版本的FileProvider问题
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }
    }

    public static AppApplication getInstance() {
        return mContext;
    }

    //获取当前版本号和版本名称
    private void getAppVersion() {
        try {
            //getPackageName()是当前类的包名，0代表是获取版本信息
            PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);
            //版本名称
            mVersionName = pi.versionName;
            //版本号
            mVersionCode = pi.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        LogUtils.d("Application-", "【版本名】:" + mVersionName + "/【版本号】" + mVersionCode);
    }



    public static void setHttpToken(String token) {
        LogUtils.e("setHttpToken-token=", token);
        HttpHeaders headers = OkGo.getInstance().getCommonHeaders();
        if (headers == null) {
            headers = new HttpHeaders();
        }
        headers.put("Access-Token", token);    //header不支持中文，不允许有特殊字符
        OkGo.getInstance().addCommonHeaders(headers);
    }

}
