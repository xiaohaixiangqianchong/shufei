package com.ubestkid.kidrhymes.view;

import android.app.Activity;

import com.hisavana.common.bean.TAdRequestBody;
import com.hisavana.mediation.ad.TVideoAd;
import com.ubestkid.kidrhymes.presenter.STAdManager;
import com.ubestkid.kidrhymes.utils.STAdUtil;

public class STVideoAd extends TVideoAd {
    String originalSlotId;
    public STVideoAd(Activity activity, String s) {
        super(activity, s);
        originalSlotId = s;
        this.j = STAdManager.getInstance().getSlotId(s);
    }

    @Override
    public void setRequestBody(TAdRequestBody tAdRequestBody) {
        super.setRequestBody(tAdRequestBody);
        STAdUtil.TAdAlliance tAdListener = (STAdUtil.TAdAlliance) tAdRequestBody.getAdListener();
        if (tAdListener != null) {
            tAdListener.onCreate(originalSlotId, false);
        }
    }
}
